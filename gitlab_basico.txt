CREAR PROYECTO GITLAB

1.- crear proyecto en la pagina de gitlab con tu cuenta, dejarlo público
2.- generar llave en git bash de tu pc (ssh-keygen -t rsa -b 2048 -C "correo de la cuenta gitlab") sin contraseña solo enter
3.- ir a buscar la llave en C/usuarios/.ssh, copiar llave abriendo en bloc de notas y pegar en la pagina de gitlab/settings/ssh keys
4.- crear una carpeta e ir a dicha carpeta desde el gitbash (cd "arrastrar carpeta a la consola gitbash")
5.- una vez en esa ruta, ir a la pagina de gitlab y clonar el proyecto copiando el primer enlace del clone.
6.- escribir git clone y pegar lo del enlace de la pagina de gitlab/clone/primer enlace
7.- introducir los archivos en una carpeta dentro de la carpeta creada previamente donde se hizo el "clone" (en esa carpeta se clonó el .git, no se debe tocar)
8.- una vez dentro los archivos a subir, en la consola gitbash introducir "git add ." sin comillas, comprobar con git status que los archivos fueron reconocidos por gitbash.
9.- seguido de esto se introduce git commit -m "info de lo que se esta subiendo o modificando" respetar comillas.
10.- por último se escribe git push origin main, para subir los archivos al repositorio de git, comprobar los archivos subidos o modificados en la pagina gitlab.




RAMAS

1.- para crear una rama se debe ir a travéz de la consola gitbash a la carpeta donde esta ubicado el .git
2.- una vez dentro, ingresar el comando git branch "nombre de la nueva rama"
3.- para moverte entre ramas usar comando git checkout "nombre de la rama", verificar que cambia el nombre d esta entre paréntesis dentro de la consola gitbash.
4.- hacer cada commit y cada push desde su rama para no estreopear el codigo guardado en main.
